# Tasks
* Provide an implementation that passes all tests.
* Add parameterized tests.

# Notes
You can use [Guava](https://github.com/google/guava).
